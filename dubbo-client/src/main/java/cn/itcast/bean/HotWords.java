package cn.itcast.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/21 14:15
 */
@Data
public class HotWords implements Serializable {

    private String hotwordsId;
    // 热词
    private String hotwords;
    // 热词类型 1:内容分类礼物(篮球) 2:内容分类礼物(足球) 10000:内容分类礼物(娱乐) 1000:默认全场礼物 1001:指定专属礼物
    private Integer type;
    // 1默认 0非默认
    //private Boolean isDefault = false;
    // 礼物顺序
    private Integer order;
    // 创建时间
    private Date createTime = new Date();
}
