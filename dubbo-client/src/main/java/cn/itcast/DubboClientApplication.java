package cn.itcast;

import cn.itcast.service.IDubboDemoService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 10:43
 */
@SpringBootApplication
public class DubboClientApplication {


    @Reference(version = "2.0.0")
    public IDubboDemoService dubboDemoService;

    public static void main(String[] args) {

        SpringApplication.run(DubboClientApplication.class, args);
    }

    @PostConstruct
    public void init() {
        String sayHello = dubboDemoService.helloDubbo();
        System.err.println(sayHello);
    }
}
