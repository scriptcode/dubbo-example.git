package cn.itcast.service;

import cn.itcast.bean.HotWords;

import java.util.List;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/17 10:14
 */
public interface IDemoService {
    String test();

    List<HotWords> hotwordsListByConf(List<String> ids);

    public List<HotWords> hotwordsList(int type) ;

    String getRedis(String key);
}
