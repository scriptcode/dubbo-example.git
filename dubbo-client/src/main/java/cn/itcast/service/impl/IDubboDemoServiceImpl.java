package cn.itcast.service.impl;

import cn.itcast.bean.HotWords;
import cn.itcast.service.IDubboDemoService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/17 10:06
 */
@Service
public class IDubboDemoServiceImpl implements IDubboDemoService {
    @Override
    public String helloDubbo() {
        return "hello dubbo, I'm server!";
    }
    @Override
    public List<HotWords> hotwordsListByConf(List<String> ids) {
        return null;
    }

    @Override
    public List<HotWords> hotwordsList(int type) {
        HotWords words=new HotWords();
        words.setHotwords("sdfsdf");
        words.setOrder(1);
        words.setType(1);
        return Arrays.asList(words);
    }

    @Override
    public String getRedis(String key) {
        return null;
    }
}
