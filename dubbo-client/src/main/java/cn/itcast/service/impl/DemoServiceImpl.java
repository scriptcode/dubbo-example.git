package cn.itcast.service.impl;

import cn.itcast.bean.HotWords;
import cn.itcast.service.IDemoService;
import cn.itcast.service.IDubboDemoService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/17 10:14
 */
@Service
public class DemoServiceImpl implements IDemoService {

   //@Reference(url = "dubbo://169.254.237.176:20880")
    @Reference(version = "2.0.0")
    public IDubboDemoService dubboDemoService;

    @Override
    public String test() {

        return    dubboDemoService.helloDubbo();
    }

    @Override
    public List<HotWords> hotwordsListByConf(List<String> ids) {
        return dubboDemoService.hotwordsListByConf(ids);
    }

    @Override
    public List<HotWords> hotwordsList(int type) {
        return dubboDemoService.hotwordsList(type);
    }

    @Override
    public String getRedis(String key) {
        return dubboDemoService.getRedis(key);
    }
}
