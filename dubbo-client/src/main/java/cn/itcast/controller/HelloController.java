package cn.itcast.controller;


import cn.itcast.service.IDemoService;
import com.alibaba.dubbo.common.extension.Activate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 10:45
 */
@RestController
public class HelloController {

    @Autowired(required = false)
    private IDemoService demoService;


    @RequestMapping("/test.do")
    @ResponseBody
    public String test() {
        System.out.println("请求");

        return demoService.test();
    }

}
