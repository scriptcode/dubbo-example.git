package com.migu.producer.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.migu.dubbo.service.TestService;
import org.springframework.stereotype.Component;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 16:59
 */
@Service(interfaceClass = TestService.class, timeout = 2000, version = "1.0.0")
@Component
public class TestServiceImpl implements TestService {
    @Override
    public String test(String name) {
        return "this is "+name+"hello 消费者！";
    }
}
