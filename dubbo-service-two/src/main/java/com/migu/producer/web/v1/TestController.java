package com.migu.producer.web.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: TestController
 * @Description TODO :
 * @Author: lhb
 * @Date: 2019-01-17 14:03
 * @Version 1.0
 **/
@RestController
public class TestController {


    @GetMapping(value = "/test")
    public String test() {
        return "Hello Producer";
    }

}
