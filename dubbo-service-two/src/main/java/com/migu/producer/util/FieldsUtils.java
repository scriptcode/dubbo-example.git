package com.migu.producer.util;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.List;

public class FieldsUtils {

	private static DBObject ContAllContAllFields = new BasicDBObject();
	private static DBObject contSubFields = new BasicDBObject();

	public static DBObject getContAllContAllFields() {
		return ContAllContAllFields;
	}

	public static DBObject buildFieldsFromList(List<String> fieldsList) {
		DBObject fields = new BasicDBObject();
		
		for(String str: fieldsList){
			fields.put(str, 1);
	
		}
		return fields;
	}

	public static DBObject buildPlayBillFields(List<String> fieldsList) {
		DBObject fields = new BasicDBObject();

		for(String str: fieldsList){
			fields.put(str, 1);

		}
		fields.put("_id", 1);
		return fields;
	}

	
	public static DBObject getContSubFields() {
		return contSubFields;
	}

	static {
		ContAllContAllFields = buildSpecContAllFields();
		contSubFields = subBuildSpecContAllFields();
	}



	private static DBObject buildSpecContAllFields() {
		DBObject fields = new BasicDBObject();
		fields.put("_id", 1);
		fields.put("content.contId", 1);
		fields.put("content.name", 1);
		fields.put("content.publishTime", 1);
		fields.put("content.isupdating", 1);
		fields.put("content.prdpackId", 1);

		fields.put("content.fields.PICTURE.lowResolutionH", 1);
		fields.put("content.fields.PICTURE.lowResolutionV", 1);
		fields.put("content.fields.PICTURE.highResolutionH", 1);
		fields.put("content.fields.PICTURE.highResolutionV", 1);

		fields.put("content.fields.Detail", 1);
		fields.put("content.fields.AssetID", 1);
		fields.put("content.fields.UDID", 1);
		fields.put("content.fields.FORMTYPE", 1);
		fields.put("content.fields.DISPLAYTYPE", 1);
		fields.put("content.fields.SerialCount", 1);
		fields.put("content.isupdating", 1);
		fields.put("content.fields.CDuration", 1);
		fields.put("content.fields.TYPE", 1);
		fields.put("content.fields.defaultMediaSize", 1);
		fields.put("content.fields.SubAlbum_IDS", 1);
		fields.put("content.fields.SubSerial_IDS", 1);
		fields.put("content.fields.Recommendation", 1);
		fields.put("content.fields.KEYWORDS", 1);
		fields.put("content.fields.SerialSequence", 1);

		fields.put("content.fields.propertyFileLists.内容形态.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.内容形态.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.内容形态.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.内容形态.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.语言.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.语言.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.语言.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.语言.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.国家及地区.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.国家及地区.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.国家及地区.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.国家及地区.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.字幕语言.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.字幕语言.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.字幕语言.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.字幕语言.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.播出平台.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.播出平台.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.播出平台.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.播出平台.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.来源.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.来源.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.来源.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.来源.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.评分.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.评分.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.评分.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.评分.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.导演.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.导演.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.导演.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.导演.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.主演.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.主演.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.主演.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.主演.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.播出年代.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.播出年代.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.播出年代.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.播出年代.propertyShow", 1);

		fields.put("content.fields.propertyFileLists.内容类型.propertyKey", 1);
		fields.put("content.fields.propertyFileLists.内容类型.propertyValue", 1);
		fields.put("content.fields.propertyFileLists.内容类型.propertyItem", 1);
		fields.put("content.fields.propertyFileLists.内容类型.propertyShow", 1);

		fields.put("content.fields.CopyRight.copyRightObjectID", 1);
		fields.put("content.fields.CopyRight.n_CPID", 1);
		fields.put("content.fields.CopyRight.name", 1);
		fields.put("content.fields.CopyRight.beginDate", 1);
		fields.put("content.fields.CopyRight.endDate", 1);
		fields.put("content.fields.CopyRight.area", 1);
		fields.put("content.fields.CopyRight.terminal", 1);
		fields.put("content.fields.CopyRight.scarcity", 1);
		fields.put("content.fields.CopyRight.way", 1);
		fields.put("content.fields.CopyRight.publish", 1);
		fields.put("content.fields.CopyRight.scope", 1);
		fields.put("content.fields.CopyRight.output", 1);
		fields.put("content.fields.CopyRight.feeType", 1);
		fields.put("content.fields.CopyRight.optimal", 1);
		fields.put("content.fields.CopyRight.needDrm", 1);

		fields.put("contentChargeInfo.products.name", 1);
		fields.put("contentChargeInfo.products.productID", 1);
		fields.put("contentChargeInfo.products.chargeMode", 1);
		fields.put("contentChargeInfo.products.price", 1);
		fields.put("contentChargeInfo.tip.code", 1);
		fields.put("contentChargeInfo.tip.msg", 1);

		fields.put("status", 1);
		return fields;
	}

	private static DBObject subBuildSpecContAllFields() {
		DBObject fields = new BasicDBObject();
		fields.put("_id", 1);
		fields.put("content.contId", 1);
		fields.put("content.name", 1);
		fields.put("content.publishTime", 1);

		fields.put("contentChargeInfo.products.name", 1);
		fields.put("contentChargeInfo.products.productID", 1);
		fields.put("contentChargeInfo.products.chargeMode", 1);
		fields.put("contentChargeInfo.products.price", 1);
		fields.put("contentChargeInfo.tip.code", 1);
		fields.put("contentChargeInfo.tip.msg", 1);

		fields.put("status", 1);
		return fields;
	}

}
