package com.migu.producer.util;

public class StringUtil {
	public static String nullToString(String value) {
		return value == null || "null".equals(value) ? "" : value.trim();
	}

	public static String nullToString(Object value) {
		return value == null ? "" : value.toString();
	}

    public static Long stringToLong(String value) {
        Long l;
        value = nullToString(value);
        if ("".equals(value)) {
            l = 0L;
        } else {
            try {
                l = Long.valueOf(value);

            } catch (Exception e) {
                l = 0L;
            }
        }
        return l;
    }
}
