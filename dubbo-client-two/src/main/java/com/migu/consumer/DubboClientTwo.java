package com.migu.consumer;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 17:26
 */
@ComponentScan(basePackages = {"com.migu.consumer.*"})
@EnableDubboConfiguration
@SpringBootApplication
public class DubboClientTwo {

    public static void main(String[] args) {
        SpringApplication.run(DubboClientTwo.class,args);
    }
}