package com.migu.consumer.controller;

import com.migu.consumer.service.TestService;
import com.migu.consumer.service.impl.TestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 17:13
 */
@RestController
public class TestController {
    @Autowired
    TestServiceImpl testService;


    @GetMapping(value = "/test/{name}")
    public String test(@PathVariable String name) {
        return testService.test(name);
    }


}
