package com.migu.consumer.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.migu.dubbo.service.TestService;
import org.springframework.stereotype.Service;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 17:10
 */

@Service
public class TestServiceImpl  {

    @Reference(check = false, timeout = 1500, version = "1.0.0", connections = 1)
    public TestService testService;

    public   String test(String name){
        return testService.test(name);
    }
}
