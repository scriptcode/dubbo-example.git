package com.migu.consumer.service;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 17:12
 */
public interface TestService {

    String test(String name);
}
