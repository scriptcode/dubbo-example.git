package cn.itcast.service.impl;


import cn.itcast.service.IDubboDemoService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/17 10:06
 */
//@Service(interfaceClass = IDubboDemoService.class)
@Service(version = "2.0.0")
public class IDubboDemoServiceImpl implements IDubboDemoService {

    @Override
    public String helloDubbo() {
        return "hello dubbo, 我是提供者erer";
    }


}
