package cn.itcast;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description :
 * @Auther : zj
 * @Date : 2019/1/23 10:36
 */
@SpringBootApplication
@DubboComponentScan(basePackages = "cn.itcast.service")
public class DubboServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboServiceApplication.class,args);
    }
}
