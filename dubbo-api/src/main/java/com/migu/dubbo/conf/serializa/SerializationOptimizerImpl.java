package com.migu.dubbo.conf.serializa;

//import com.alibaba.dubbo.common.serialize.support.SerializationOptimizer;

import com.alibaba.dubbo.common.serialize.support.SerializationOptimizer;
import com.migu.dubbo.entity.UserInfo;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName: SerializationOptimizerImpl
 * @Description TODO : 需要被序列化的类注册到dubbo系统中
 * @Author: lhb
 * @Date: 2019-01-17 16:20
 * @Version 1.0
 **/
public class SerializationOptimizerImpl implements SerializationOptimizer {

    public Collection<Class> getSerializableClasses() {
        List<Class> classes = new LinkedList<Class>();
        classes.add(UserInfo.class);
        return classes;
    }
}
