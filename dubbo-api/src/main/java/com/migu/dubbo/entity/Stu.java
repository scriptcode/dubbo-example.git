package com.migu.dubbo.entity;

import java.io.Serializable;

/**
 * @ClassName: Stu
 * @Description TODO :
 * @Author: lhb
 * @Date: 2019-01-17 17:11
 * @Version 1.0
 **/
public class Stu implements Serializable {

    private static final long serialVersionUID = 1213456786543L;

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Stu(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Stu() {
    }
}
