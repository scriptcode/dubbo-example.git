package com.migu.dubbo.entity;

import java.io.Serializable;

/**
 * @ClassName: UserInfo
 * @Description TODO :
 * @Author: lhb
 * @Date: 2019-01-17 16:05
 * @Version 1.0
 **/
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userName;
    private int age;
    private String desc;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public UserInfo(String userName, int age, String desc) {
        this.userName = userName;
        this.age = age;
        this.desc = desc;
    }

    public UserInfo() {
    }
}
