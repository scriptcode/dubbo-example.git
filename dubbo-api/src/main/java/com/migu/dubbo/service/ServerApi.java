package com.migu.dubbo.service;

import com.migu.dubbo.entity.Stu;
import com.migu.dubbo.entity.UserInfo;

import java.util.List;

/**
 * Created by ZSX on 2018/9/12.
 * @author ZSX
 */
public interface ServerApi {


    String formatUUID(String uuid);

    List<String> findListStr(int num);

    UserInfo userInfo();

    List<UserInfo> userInfoList();

    Stu stu();

    Stu stu(Stu stu);

}
